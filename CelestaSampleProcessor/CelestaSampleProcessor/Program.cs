﻿// For each sample, this program normalizes its peak volume, and adds a small
// fade-in to that peak volume so that the start of the sample is always 0.
// In addition, it tries to run a gate over each file to reduce background
// noise - it tries to fit an exponential curve to the values of the audio
// above a noise threshold (maybe determined from the end of each file?), then
// extrapolates that curve as a volume envelope to the rest of the file, making
// sure only to attenuate the audio signal.
// Finally, it determines the actual pitch of the sound and speeds up or slows
// down the file so that it matches equal temperament tuning.

// Or, possibly - 
// Read the file
// Apply volume enveloping
// Determine pitch
// Use a custom ISampleProvider class to change the speed and save at 48 kHz

using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelestaSampleProcessor
{
	// Used for interpreting a sequence of floating-point values using a
	// different sampling rate, for speeding up and slowing down audio.
	class SimpleSampleProvider : ISampleProvider
	{
		public WaveFormat WaveFormat { get; }
		float[] internalSamples;

		public int Read(float[] buffer, int offset, int count)
		{
			int samplesRead = Math.Min(count, Math.Max(0, internalSamples.Length - offset));
			for(int sample = 0; sample < samplesRead; sample++)
			{
				buffer[sample] = internalSamples[sample + offset];
			}
			return samplesRead;
		}

		public SimpleSampleProvider(float[] samples, int numChannels, int sampleRate)
		{
			internalSamples = samples;
			WaveFormat = new WaveFormat(sampleRate, 32, numChannels);
		}
	}

	class Program
	{
		// Compute the peaks of the file, returning a list of sample indexes.
		static List<int> ComputePeaks(ref float[] monoSamples, int sampleRate)
		{
			// Let's attempt to do this using a simple envelope follower.
			// We'll say that a peak is the last sample before the envelope
			// follower begins to coast above the signal.
			float envelopeAttenuationPerSecond = 1e-3f; // Ad-hoc
			float envelopeAttenuationPerSample = (float)Math.Pow(envelopeAttenuationPerSecond, 1.0f / sampleRate);

			List<int> result = new List<int>();

			float envelope = 0.0f;
			bool wasOnEnvelope = false;
			for(int sample = 0; sample < monoSamples.Length; sample++)
			{
				envelope *= envelopeAttenuationPerSample;

				float absValue = Math.Abs(monoSamples[sample]);
				if (absValue > envelope)
				{
					envelope = absValue;
					wasOnEnvelope = true;
				}
				else
				{
					if (wasOnEnvelope)
					{
						result.Add(sample - 1);
					}
					wasOnEnvelope = false;
				}
			}

			return result;
		}

		// Normalize the audio to have a peak of 0.8f.
		static void NormalizeAudio(ref float[] interleavedSamples)
		{
			// Normalize the audio to have a peak of 0.8f
			float maxAbsValue = 0.0f;
			foreach (float value in interleavedSamples)
			{
				maxAbsValue = Math.Max(maxAbsValue, Math.Abs(value));
			}
			float multiplier = 0.8f / maxAbsValue;
			for (int i = 0; i < interleavedSamples.Length; i++)
			{
				interleavedSamples[i] *= multiplier;
			}
		}

		static void ExportStereoAudio(ref float[] interleavedSamples, string outfile, int sampleRate)
		{
			string outDirectory = Path.GetDirectoryName(outfile);
			Directory.CreateDirectory(outDirectory);

			WaveFormat waveFormat = new WaveFormat(sampleRate, 24, 2);
			WaveFileWriter waveFile = new WaveFileWriter(outfile, waveFormat);
			waveFile.WriteSamples(interleavedSamples, 0, interleavedSamples.Length);
			waveFile.Close();
		}

		static double ComputeToneScore(ref float[] monoSamples, float testHz, int sampleRate, int frames)
		{
			double scoreReal = 0.0;
			double scoreImag = 0.0;
			// exp(2*i*pi*hz*(sample/samplesPerSecond))
			double multiplier = 2 * Math.PI * testHz / sampleRate;
			for (int frame = 0; frame < frames; frame++)
			{
				scoreReal += Math.Cos(multiplier * frame) * monoSamples[frame];
				scoreImag += Math.Sin(multiplier * frame) * monoSamples[frame];
			}
			return scoreReal * scoreReal + scoreImag * scoreImag;
		}

		static void Main(string[] args)
		{
			// This is simple enough that we'll write it in a single function.
			foreach(string filename in Directory.EnumerateFiles(
				"../../../../../ReaperOut",
				"*.wav",
				SearchOption.AllDirectories))
			{
				Console.WriteLine(filename);
				// Load all of the audio from the file at once
				int sampleRate;
				int channels;
				float[] interleavedSamples;
				{
					WaveFileReader wavFile = new WaveFileReader(filename);
					// Automatically converts to float from PCM samples.
					ISampleProvider reader = wavFile.ToSampleProvider();

					WaveFormat format = wavFile.WaveFormat;
					sampleRate = format.SampleRate;
					channels = format.Channels;
					if(channels != 1 && channels != 2)
					{
						Console.WriteLine("Error: The number of channels in " +
							"{} was {}, but the file has to be mono or stereo!",
							filename, channels);
					}
					// The number of samples in all of the audio channels combined.
					// Note that this will fail if there are more than 2^31-1
					// samples in the file, which shouldn't happen with these
					// celesta samples (as it would amount to a sample that is
					// more than (2^31-1)/(2*48000*3600) = 6 hours long)
					int numSamples = Convert.ToInt32(
						wavFile.Length / (format.BitsPerSample / 8));
					interleavedSamples = new float[numSamples];

					reader.Read(interleavedSamples, 0, numSamples);
				}

				// The number of frames of the WAV file (each of which has 
				// `channels` samples)
				int frames = interleavedSamples.Length / channels;

				// Generate a monophonic form of the audio to compute
				// volume envelopes by averaging the left and right channels
				float[] monoSamples = new float[frames];
				for(int frame = 0; frame < frames; frame++)
				{
					monoSamples[frame] = 0.5f * (
						interleavedSamples[2 * frame + 0]
						+ interleavedSamples[2 * frame + 1]);
				}

				// Compute peaks
				List<int> peaks = ComputePeaks(ref monoSamples, sampleRate);

				// True if this audio file represents a note, instead of a
				// sound effect
				string filenameOnly = Path.GetFileName(filename);
				bool isNote = !(filenameOnly.StartsWith("creak")
					|| filenameOnly.StartsWith("keyRelease")
					|| filenameOnly.StartsWith("pedal"));

				// Apply an envelope to the tail of the sound
				if(isNote){
					// For the peaks above a given noise level, approximate
					// their values with a function of the form y_i = Ae^(k*x_i),
					// where A and k are some constants. We do this by
					// minimizing sum((log(y_i) - log(A e^(k*x_i)))^2).
					// I'll skip the derivation here and go directly to the
					// solution (note that this probably has numerical issues).

					// Previously, we used only a noise threshold, but we'll instead
					// use a threshold in terms of samples.
					float sampleStart = 400;
					float sampleEnd = 48000 * 3 / 2; // i.e. 1.5 seconds at 48000 Hz
					float noiseThreshold = 0.005f;

					double sum1 = 0.0f;
					double sumX = 0.0f;
					double sumXSquared = 0.0f;
					double sumLogY = 0.0f;
					double sumXLogY = 0.0f;

					foreach (int x in peaks)
					{
						float y = Math.Abs(monoSamples[x]);
						// Ignore samples below the noise threshold as well as
						// the first 400 samples, as these usually are part
						// of the attack
						if(sampleStart <= x && x < sampleEnd && y > noiseThreshold)
						{
							double logY = Math.Log(y);
							sum1 += 1;
							sumX += x;
							sumXSquared += (float)x * (float)x;
							sumLogY += logY;
							sumXLogY += x * logY;
						}
					}

					double oneOverDet = 1.0f / (sum1 * sumXSquared - sumX * sumX);
					double logA = oneOverDet * (sumXSquared * sumLogY - sumX * sumXLogY);
					float a = (float)Math.Exp(logA);
					float k = (float)(oneOverDet * (-sumX * sumLogY + sum1 * sumXLogY));

					// Sanity checks
					if(oneOverDet < 0.0f || double.IsInfinity(oneOverDet) || double.IsNaN(oneOverDet))
					{
						Console.WriteLine("Error: oneOverDet was "
							+ oneOverDet
							+ ", but should be a positive finite number!");
						return;
					}

					if(k >= 0.0f)
					{
						Console.WriteLine("Error: k in envelope approximation was "
							+ k + ", but should be negative!");
						return;
					}

					Console.WriteLine("Envelope: a = {0}, k = {1}", a, k);

					// Compute the amount of attenutation for each peak. For
					// peaks above the noise threshold, this will be 1, while
					// we choose values that will clamp peaks below the noise
					// threshold to the exponential curve. This ends with 0
					// to make the next block simpler.
					float[] peakAttenuation = new float[peaks.Count + 1];
					for (int i = 0; i < peaks.Count; i++)
					{
						int x = peaks[i];
						float y = Math.Abs(monoSamples[x]);
						if (y > noiseThreshold)
						{
							peakAttenuation[i] = 1.0f;
						}
						else
						{
							float envelopeValue = (float)(a * Math.Exp(k * x));
							peakAttenuation[i] = Math.Min(1.0f, envelopeValue / y);
						}
						if (i > 0)
						{
							// Make it so that the envelope only goes downwards
							peakAttenuation[i] = Math.Min(peakAttenuation[i], peakAttenuation[i - 1]);
						}
					}
					peakAttenuation[peaks.Count] = 0.0f;

					// Apply the volume envelope defined by peakAttenuation to
					// the stereo audio. Also include a ramp up to the first
					// peak.
					
					// Linear ramp up to the first peak (usually in the attack
					// of the sound)
					for(int frame = 0; frame < peaks[0]; frame++)
					{
						float attenuation = ((float)frame) / peaks[0];
						interleavedSamples[2 * frame + 0] *= attenuation;
						interleavedSamples[2 * frame + 1] *= attenuation;
					}
					// Linear interpolation through peakAttenuation
					for(int peak = 0; peak < peaks.Count; peak++)
					{
						int frameStart = peaks[peak];
						int frameEnd = (peak == peaks.Count - 1 ? monoSamples.Length : peaks[peak + 1]);
						float attenuationStart = peakAttenuation[peak];
						float attenuationEnd = peakAttenuation[peak + 1];
						if(attenuationStart == 1.0f && attenuationEnd == 1.0f)
						{
							// Nothing to do
						}
						else
						{
							for(int frame = frameStart; frame < frameEnd; frame++)
							{
								float t = (frame - frameStart) / ((float)(frameEnd - frameStart));
								float attenuation = attenuationStart + t * (attenuationEnd - attenuationStart);
								interleavedSamples[2 * frame + 0] *= attenuation;
								interleavedSamples[2 * frame + 1] *= attenuation;
							}
						}
					}
				}

				// Export the untuned file
				NormalizeAudio(ref interleavedSamples);
				{
					string outfile = filename.Replace("ReaperOut", "Samples\\Untuned");
					ExportStereoAudio(ref interleavedSamples, outfile, sampleRate);
				}

				// Determine the actual pitch of the audio and retune it so
				// that it closely matches equal temperament to within 0.05 Hz:
				// Uses A4 = 440 Hz.
				if(isNote)
				{
					// Recompute the monophonic audio
					for (int frame = 0; frame < frames; frame++)
					{
						monoSamples[frame] = 0.5f * (
							interleavedSamples[2 * frame + 0]
							+ interleavedSamples[2 * frame + 1]);
					}

					// Parse what note the file should be from the filename
					string[] noteNames = { "A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#" };
					int note = -1;
					for (int name = 0; name < noteNames.Length; name++)
					{
						if (filenameOnly.StartsWith(noteNames[name]))
						{
							note = name;
						}
					}
					if(note == -1)
					{
						Console.WriteLine("Error: Was unable to determine the "
							+ "note from the filename, " + filenameOnly + "!");
						return;
					}

					int octave;
					if(!int.TryParse(filenameOnly.Substring(noteNames[note].Length,1),out octave))
					{
						Console.WriteLine("Error: Was unable to get the octave "
							+ "from the filename, " + filenameOnly + "!");
						return;
					}

					// Ideal equal-temperament note pitch
					float idealHz = 440.0f * (float)Math.Pow(2.0f, (note / 12.0f) + (float)(octave - 4));

					// To determine the pitch of the file, we'll do hill climbing
					// in steps of .1 Hz. Note that there are *much* faster
					// ways of doing this! This is intended to be slow but accurate.

					const float stepHz = 0.1f;
					float leftHz = idealHz;
					float rightHz = idealHz + stepHz;
					double leftScore = ComputeToneScore(ref monoSamples, leftHz, sampleRate, frames);
					double rightScore = ComputeToneScore(ref monoSamples, rightHz, sampleRate, frames);

					float bestHz = idealHz;
					if(rightScore > leftScore)
					{
						while(rightScore > leftScore)
						{
							leftHz = rightHz;  leftScore = rightScore;
							rightHz += stepHz; rightScore = ComputeToneScore(ref monoSamples, rightHz, sampleRate, frames);
						}
						bestHz = leftHz;
					}
					else
					{
						while(leftScore > rightScore)
						{
							rightHz = leftHz; rightScore = leftScore;
							leftHz -= stepHz; leftScore = ComputeToneScore(ref monoSamples, leftHz, sampleRate, frames);
						}
						bestHz = rightHz;
					}

					Console.WriteLine("Measured pitch = {0} Hz, equal-temperament pitch = {1} Hz",
						bestHz, idealHz);

					// Sanity check
					if(Math.Abs(bestHz - idealHz)/idealHz > 0.04f)
					{
						Console.WriteLine("Error: The measured pitch was too "
							+ "far off from the labeled pitch! Is this file "
							+ "labeled correctly, or did the pitch estimator "
							+ "determine the wrong pitch?");
						return;
					}

					// Now, change the pitch of the file by slowing it down
					// or speeding it up - by interpreting it as having a
					// different sample rate, and resampling it to 48 kHz.
					int modifiedSampleRate = (int)((idealHz / bestHz) * sampleRate);
					SimpleSampleProvider sampleProvider = new SimpleSampleProvider(interleavedSamples, channels, modifiedSampleRate);

					// Now the sample rate is 48 kHz.
					sampleRate = 48000;
					WdlResamplingSampleProvider resampler = new WdlResamplingSampleProvider(sampleProvider, sampleRate);

					int numNewSamples = (int)((interleavedSamples.Length * (float)sampleRate) / modifiedSampleRate);
					numNewSamples = 2 * ((numNewSamples + 1) / 2); // Round up to multiple of 2
					float[] newSamples = new float[numNewSamples];
					resampler.Read(newSamples, 0, numNewSamples);

					interleavedSamples = newSamples;
				}

				// Export the tuned file
				{
					NormalizeAudio(ref interleavedSamples);
					{
						string outfile = filename.Replace("ReaperOut", "Samples\\Tuned");
						ExportStereoAudio(ref interleavedSamples, outfile, sampleRate);
					}
				}
			}

			Console.WriteLine("Done!");
		}
	}
}
