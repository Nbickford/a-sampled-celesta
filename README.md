# A Sampled Celesta

![An image showing the recording configuration used for the celesta](docs/celesta-image.png)

A free, fully-sampled virtual celesta. Includes soundfont, FX chains, and Sforzando files. This is the same celesta as played by Grant Huang [here](https://www.youtube.com/watch?v=_-92lvJd3g4)!

**Download the Soundfont [here](https://drive.google.com/file/d/17jdqQJO4BJDg4dD06P50UJNa77dLnrpX/view?usp=sharing)!**

You can also download the full set of samples, including **SFZ (Sforzando) files** and a **REAPER ReaSamplOmatic 5000 FX chain**, [here](https://drive.google.com/drive/folders/1oNmgTpChArDIHntQdiv4_CqwrszYkYn7?usp=sharing)! For most purposes, you'll want to download one of the tuned, denoised sample packs - whether in coincident (XY), mixed, or in wide (spaced pair) stereo configurations, but all 12 sample packs are available there.

New sample packs and presets using this celesta are also welcomed!

### Sample Tracks

Each of these tracks uses a single instance of the celesta using the tuned denoised mix configuration and an instance of Soundtoys' Little Plate for reverb.

[Edvard Grieg, Anitra's Dance](https://drive.google.com/open?id=1kq7TBCSmyJHb3hImsWHstwNM6xyoA6F4)

[Claude Debussy, Clair de Lune](https://drive.google.com/file/d/1QYAtvRMDoLpuE1utk4ZpJ1Tdn_cG7AMw/view?usp=sharing)

[Edvard Grieg, In the Hall of the Mountain King](https://drive.google.com/open?id=1wYWXdo9l6q5WZ-u-imzD_PbgtLPP8uHl)

[Claude Debussy, The Snow is Dancing](https://drive.google.com/file/d/1wpVjeKjf9nbj4VIj9YerOyT-xFGVdo9X/view?usp=sharing)

[Korobeiniki (traditional)](https://drive.google.com/file/d/19oXjPuG5sI170JMy_GC5O_a9Hie3hj-q/view?usp=sharing)

The included Soundfont was played by Grant Huang in his cover of Snowbelle City from Pokémon X and Y [here](https://www.youtube.com/watch?v=pXVUKu7kSQY)!

I'm also always interested to hear about new places that this virtual celesta is being used, and may include them here.

### Configuration Instructions

For Ableton and LMMS, probably the best way to go is to use the Soundfont above.

For REAPER, Logic Pro, and Pro Tools, you'll probably want to use the Plogue Sforzando sample packs. (Logic may also be able to read Soundfont files via the instructions [here](https://www.logicprohelp.com/forum/viewtopic.php?t=120859).)

Install Sforzando from https://www.plogue.com/products/sforzando.html. Then download the directory of samples as well as the SFZ file for the configuration you want from [the releases directory here](https://drive.google.com/drive/folders/1oNmgTpChArDIHntQdiv4_CqwrszYkYn7?usp=sharing), and extract them into the same folder. For instance, if you're using the Tuned Denoised Mix preset, your folder would look like this:

```
📁 Tuned/
   └─📁 Denoised Mix/
        ├─📄 A#4 rr1.wav
        └─📄 ...
📄 Celesta Tuned Denoised Mix.sfz
```

while if you're using the Untuned Wide preset, your folder would look like this:

```
📁 Untuned/
   └─📁 Wide/
        ├─📄 A#4 rr1.wav
        └─📄 ...
📁 Celesta Untuned Wide.sfz
```

You can also download all of the samples at once, in which case your folder would look like this:

```
📁 Tuned/
   └─📁 ...
📁 Untuned/
   └─📁 ...
📄 Celesta Tuned Coincident.sfz
📄 ...
📄 Celesta Untuned Wide.sfz
```

Launch an instance of Sforzando in your digital audio workstation, and drag the .sfz file into Sforzando to load the instrument.

Tip: Sforzando uses a release time of zero seconds by default, but you can change this to mimic the effect of a sustain pedal by going to the `Controls` tab in Sforzando and dragging the `CC072` (Release Time) value to 100%. This will also react to MIDI control changes.

![Diagram showing how to go to the Controls tab in Sforzando and where to find the CC072 value](docs/sforzando-release-setup.png)

Alternatively, you can also turn sustain on or off using the rightmost sustain pedal button in Sforzando:

![The sustain pedal is at the bottom-right-hand corner of the screen.](docs/sforzando-release-2.png)

## About the Samples

This is a Mustel celesta where every key has been recorded with two microphone setups. Although the original celesta spans 4 octaves (C4-C8), the sample packs use some different techniques to expand this to the larger 5 1/2-octave celesta's range (C3-F8, A4 = 440 Hz). Here's a feature summary:

* Two stereo recording configurations (coincident X/Y recorded using a Zoom H4n Pro and spaced pair using a Blue Baby Bottle and a Sterling ST51), as well as mixed samples (details below).
* Most notes have 1 sample, but some notes have up to 3 round-robin samples.
* Additional samples for note-up, pedal-down, pedal-up, and creaks.
* Samples available both before and after denoising.
* Re-tuned versions of the samples tuned to within .01 Hz of 12-tone equal temperament available.
* A new processing algorithm to adaptively gate a sample based on fitting its volume envelope.

In total, there are 12 sample sets:

* Coincident (XY), wide (spaced pair), and mixed mic combinations
* Denoised and original samples
* Tuned and original samples (as many of the original's notes are off from equal temperament)

This project currently provides pre-made sample packs for each of these configurations using Plogue's free [Sforzando sampler](https://www.plogue.com/products/sforzando.html), and Soundfont (Ableton, TX16Wx, etc.) and REAPER ReaSamplOmatic 5000 FX Chain files for the flagship configuration (tuned, denoised, mix).

The mix configurations use the same mix  of the wide and coincident pairs as the Celeste Celeste Collections videos: https://www.youtube.com/watch?v=_-92lvJd3g4

The celesta was close-miked from the back with all three microphones. The Zoom H4N Pro was used in  its 90-degree XY configuration a few inches away from the middle of the soundboard, while the Blue Baby Bottle and Sterling ST51 microphones were placed by ear next to the left and right sides of the soundboard  respectively from the view above.

If you're building your own sample pack from these samples, note that the original celesta's F5 key has a chiming sound. (It's an old celesta, and has been through many stories!) The sample packs get around this  by using transposed versions of E5 and F#5 as round-robins for F5, or by using transposed versions of E5 alone.

## About the Processing Pipeline

All of the samples and sample packs are generated from the original two stereo audio files using REAPER, a custom C# program, and a Python script. Here's a quick outline of the processing pipeline (sorry about the complexity, that's my fault!):

* The pipeline starts with `Celeste Samples Source - BlueL - StirlingR.wav` (the source for the Wide configurations) and `Celeste Samples Source - H4n Pro.wav` (the source for the Coincident configurations). These contain the sources for every sampled note from each microphone configuration, and were stitched together from two recording halves. Both are sampled at 48 kHz.
* Rendering all regions in `Sample Separation.rpp` in REAPER will mix these files together, apply EQ to the wide microphones, and also generate denoised versions using the JSFX plugin described here: https://github.com/nbickford/REAPERDenoiser. This will output files to 6 folders in `ReaperOut`, for a total of 552 files (=6*92). These regions define where each note starts and ends, and were placed mostly by hand.
* Next, the C# project `CelestaSampleProcessor` will take each of the samples, apply an adaptive gating algorithm to each of them (described below) to further reduce noise, normalize their volume, and then detect pitch and tune each of the samples to 12-tone equal temperament (A4=440 Hz), creating all 1,104 samples (=2*552) in the `Tuned` and `Untuned` directories  in `Samples`.
* Finally, let's combine these samples into sample packs! Running `python generateSforzando.py` will generate all 12 SFZ files in `Samples`. If you've already downloaded the samples, you can also modify and rerun the script to modify all of the SFZ files at once.
* The `RfxChain` sample pack was built in REAPER by constructing and exporting the ReaSamplOmatic 5000 instances as an FX chain, and the SF2 soundfont was built using the [Polyphone soundfont editor](https://www.polyphone-soundfonts.com/).

In addition to pitch estimation and volume normalization, the `CelestaSampleProcessor` implements an offline adaptive gating technique to reduce room noise that I think might be new. The idea is that we can approximate the envelope of the sustain of a celesta note by fitting an exponential curve, `f(t) = Ae^(-kt)`, to the peaks in the first 0.008-1.5 seconds of the file, as shown below:

![celesta-audio-graph](docs/celesta-audio-graph.png)

The tail of this sound is mostly room noise, and we can hide it by fading the sound out over time so that its tail (the section after 1.5 seconds, in this case) matches this exponential curve. For more details and implementation, please see the comments in [Program.cs](CelestaSampleProcessor/CelestaSampleProcessor/Program.cs).

Please let me know if you're working on regenerating the samples and have problems with the processing pipeline! If you'd like to try running your own denoising or audio processing algorithms on the samples, note that you can also download the untuned, non-denoised samples directly above without having to regenerate them.

## Credits

This celesta was recorded by [Neil Bickford](https://neilbickford.com/) in the Orchestra Room of the Herb Alpert School of Music's Schoenberg Hall at UCLA on March 8th, 2019.

[Grant Huang](https://www.linkedin.com/in/grant-huang-3b0188142/) and Samuel Neubuerger handled recording setup, and [Grant Huang](https://www.linkedin.com/in/grant-huang-3b0188142/) performed celesta for [one of the two tracks this celesta was originally recorded for](https://www.youtube.com/watch?v=_-92lvJd3g4).

[Patrick Gardner](https://patrickegardner.com) generously lent Blue Baby Bottle and Sterling Audio microphones for the recording session.

Thanks to the [Game Music Ensemble at UCLA](https://www.youtube.com/c/GameMusicEnsembleatUCLA) and the [Herb Alpert School of Music at UCLA](https://schoolofmusic.ucla.edu/) for providing the recording space.

Special thanks to [Lena Raine](https://lena.fyi), [Trevor Alan Gomes](https://trevorgomes.bandcamp.com/), and [Sebastian Wolff](https://materiacollective.com) for inspiring the Celeste Celeste Collections videos, without which the celesta would not have been recorded.

## License Info

The software in this repository is distributed under the MIT license, while audio files and presets (`.sfz`, `.sf2`, `.wav`, and `.RfxChain` files) are distributed under the CC0 1.0 Universal license. In other simplified terms, you can use and modify the software and sounds in this repository for both commercial and non-commercial projects without need for attribution (although I would be interested in hearing about where these samples have gotten to be used).

For more information, please see the [license file](LICENSE).

To the extent possible under law, [Neil Bickford](https://gitlab.com/Nbickford/a-sampled-celesta/) has waived all copyright and related or neighboring rights to A Sampled Celesta samples. This work is published from: United States.

![Public Domain](https://licensebuttons.net/p/zero/1.0/88x31.png)