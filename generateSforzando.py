# To run this file, first generate the celesta samples (using
# CelestaSampleProcessor), then run
#     python generateSforzando.py
# This script generates an SFZ file (.sfz) for playing back celesta samples
# using Plogue's Sforzando (https://www.plogue.com/products/sforzando.html).
# It's based upon the file format used for Versilian Studios Chamber Orchestra's
# (https://vis.versilstudios.com/vsco-2.html) Community Edition
# (https://github.com/sgossner/VSCO-2-CE/).
# For more information on the SFZ file format, see https://sfzformat.com/

import os

def writeSFZFile(path):
	"""
	path: The folder containing the samples to write a SFZ file for.
	"""
	name = path.replace('\\', ' ').strip()

	f = open('Samples/Celesta ' + name + '.sfz', 'w')

	f.writelines([
		'//From https://gitlab.com/Nbickford/a-sampled-celesta'+'\n',
		'<control>'+'\n',
		'default_path='+path+'\n',
		'<global>'+'\n',
		'ampeg_attack=0.0'+'\n',
		'ampeg_releasecc72=10.0'+'\n',
		'ampeg_dynamic=1'+'\n',
		'volume=0'+'\n',
		'polyphony=128'+'\n'
	])

	# To simulate a lower octave, we take the bottom three samples - C4, C#4, and D4
	# - and repeat them four times with transposition. I think this helps conceal
	# the fact that we're reusing samples for this octave.
	lowSamples = ['C4 rr1.wav', 'C#4 rr1.wav', 'D4 rr1.wav']
	note = 48
	for group in range(4):
		for sampleIndex, sample in enumerate(lowSamples):
			f.writelines([
				'<group>'+'\n',
				'<region>'+'\n',
				'sample='+sample+'\n',
				'lokey='+str(note)+'\n',
				'hikey='+str(note)+'\n',
				'pitch_keycenter='+str(60+sampleIndex)+'\n',
			])
			note += 1

	# Extend to F8 (the previous highest note was C8) using the same trick:
	highSamples = ['A#7 rr1.wav', 'B7 rr1.wav', 'C8 rr1.wav']
	note = 109 # C#8
	for group in range(2):
		for sampleIndex, sample in enumerate(highSamples):
			if note < 109+5:
				f.writelines([
					'<group>'+'\n',
					'<region>'+'\n',
					'sample='+sample+'\n',
					'lokey='+str(note)+'\n',
					'hikey='+str(note)+'\n',
					'pitch_keycenter='+str(106+sampleIndex)+'\n',
				])
			note += 1

	# Key-up sound
	f.writelines([
		'<group>'+'\n',
		'<region>'+'\n',
		'sample=keyRelease rr1.wav'+'\n',
		'lokey=48'+'\n',
		'hikey=113'+'\n',
		'trigger=release'+'\n',
		'sw_vel=current'+'\n',
		'pitch_keytrack=0'+'\n',
		'volume=-36'+'\n',
		'pitch_random=50'+'\n'
	])

	# Iterate over the files in the directory and collect them into groups.
	noteDictionary = {}

	for file in os.listdir('Samples\\' + path):
		splitFile = file.split()
		noteStr = splitFile[0]
		rrExt = splitFile[1] # (round robin and extension)
		if noteStr in noteDictionary:
			noteDictionary[noteStr].append(rrExt)
		else:
			# Create a new element
			noteDictionary[noteStr] = [rrExt]

	# Iterate over each group, parse the note it corresponds to, and write the groups.
	noteNames=['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
	for noteStr, rrExts in noteDictionary.items():
		f.write('<group>\n')
		# Parse the note name
		subNote = 12
		for noteIndex in range(11, -1, -1):
			if(noteStr.startswith(noteNames[noteIndex])):
				subNote = noteIndex
				break
		if(subNote == 12):
			continue # Sound effect
		else:
			noteOctave = int(noteStr[len(noteNames[subNote])])
			note = 12 + 12 * noteOctave + subNote
			if(note == 77):
				continue # F6 has a chime sound that we work around
			f.writelines([
				'lokey='+str(note)+'\n',
				'hikey='+str(note+1 if note == 76 else note)+'\n', # Work around chime on F6
				'pitch_keycenter='+str(note)+'\n',
				'seq_length='+str(len(rrExts))+'\n'
			])
			for seqPosition, rrExt in enumerate(rrExts):
				filename = noteStr + ' ' + rrExt
				f.writelines([
					'<region>'+'\n',
					'sample='+filename+'\n',
					'seq_position='+str(seqPosition+1)+'\n'
				])

	f.close()

allPaths = [
	'Tuned\\Coincident\\',
	'Tuned\\Denoised Coincident\\',
	'Tuned\\Denoised Mix\\',
	'Tuned\\Denoised Wide\\',
	'Tuned\\Mix\\',
	'Tuned\\Wide\\',
	'Untuned\\Coincident\\',
	'Untuned\\Denoised Coincident\\',
	'Untuned\\Denoised Mix\\',
	'Untuned\\Denoised Wide\\',
	'Untuned\\Mix\\',
	'Untuned\\Wide\\',
]
for path in allPaths:
	writeSFZFile(path)
print('Done')